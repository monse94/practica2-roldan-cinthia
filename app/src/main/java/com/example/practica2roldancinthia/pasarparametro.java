package com.example.practica2roldancinthia;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class pasarparametro extends AppCompatActivity {

    EditText txtParametro;
    Button btnEnviarParametro;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pasarparametro);

        txtParametro = (EditText)findViewById( R.id.txtParametro );
        btnEnviarParametro = (Button)findViewById( R.id.btnEnviarParametro );

        btnEnviarParametro.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent( pasarparametro.this, recibirparametro.class );
                startActivity( intent );

                Bundle bundle = new Bundle();

                bundle.putString("dato", txtParametro.getText().toString()  );

                intent.putExtras(bundle);
                startActivity( intent );
            }
        } );
    }
}
