package com.example.practica2roldancinthia;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class recibirparametro extends AppCompatActivity {

    TextView tvTexto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibirparametro);

        tvTexto= (TextView)findViewById( R.id.tvRecibirPrametro);

        Bundle bundle= this.getIntent().getExtras();
        tvTexto.setText( bundle.getString( "dato" ) );

    }
}
